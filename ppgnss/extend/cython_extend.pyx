cimport cpython.datetime as dt
import numpy as np
cimport numpy as np
from libc.stdlib cimport malloc, free
from libc.math cimport isnan

cdef list _clock2strlist(np.ndarray[double, ndim=2] data, list second_deltas, list prns, dt.datetime first_epoch):
    cdef int nr = data.shape[0]
    cdef int nc = data.shape[1]
    lines = list()
    cdef str line = ""
    for i, sec in enumerate(second_deltas):
        for j, prn in enumerate(prns):
            this_epoch = first_epoch + np.timedelta64(int(sec), 's')
            if isnan(data[i][j]):
                continue
            line = "AS %s  %s  2   %19.12e  %18.12e\n" \
            % (prn, this_epoch.strftime('%Y %m %d %H %M %S.000000'), data[i][j], 0)
            lines.append(line)
    return lines

def clock2strlist(data, secs, prns, first_epoch):
    return _clock2strlist(data, secs, prns, first_epoch)


# -*- coding: utf-8 -*-
"""
   ppgnss
   ~~~~~~

   A GNSS software written with python.

   :copyright: (c) 2017 by Liang Zhang
   :license: GNU, see LICENSE for more details.
"""

__version__ = '1.0'
import numpy as np
import pandas as pd
import xarray as xr

.. pygnss documentation master file, created by
   sphinx-quickstart on Sun Sep 24 17:07:03 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ppgnss's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 4

   pygnss
   tests

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

